import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { SensorReading } from '../readings.model';
import { ReadingsService } from '../readings.service';

type ColumnId = keyof SensorReading;

interface TableColumn {
  id: ColumnId;
  name: string;
  render: (row: SensorReading) => string | number;
  filter?: boolean;
  sortable?: boolean;
}

@Component({
  selector: 'app-readings',
  templateUrl: './readings.component.html',
  styleUrls: ['./readings.component.scss']
})
export class ReadingsComponent implements OnInit {
  columns: TableColumn[] = [
    { id: 'id', name: 'ID', render: row => row.id },
    { id: 'box_id', name: 'Box ID', render: row => row.box_id },
    { id: 'sensor_type', name: 'Sensor Type', render: row => row.sensor_type, filter: true, sortable: true },
    { id: 'unit', name: 'Unit', render: row => row.unit },
    { id: 'name', name: 'Name', render: row => row.name, filter: true },
    { id: 'range_l', name: 'Lower Bound', render: row => row.range_l },
    { id: 'range_u', name: 'Upper Bound', render: row => row.range_u },
    { id: 'longitude', name: 'Longitude', render: row => row.longitude },
    { id: 'latitude', name: 'Latitude', render: row => row.latitude },
    { id: 'reading', name: 'Reading', render: row => row.reading },
    { id: 'reading_ts', name: 'Taken At', render: row => row.reading_ts, sortable: true },
  ];
  displayedColumns: string[] = this.columns.map(o => o.id);
  dataSource: MatTableDataSource<SensorReading>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private readingsService: ReadingsService) {
  }

  ngOnInit() {
    this.readingsService.getReadings().subscribe(
      (data) => {
        this.dataSource = new MatTableDataSource<SensorReading>(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.dataSource.filterPredicate = (src, filter: string) => {
          return this.columns.filter(o => o.filter === true).some(col => src[col.id].toString().toLowerCase().includes(filter));
        };
      });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  isSortingDisabled(column: TableColumn) {
    return !this.columns.some((o) => o.id === column.id && o.sortable === true);
  }

}
