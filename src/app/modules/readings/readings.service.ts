import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { SensorReading } from './readings.model';

@Injectable({
  providedIn: 'root'
})
export class ReadingsService {
  private readings$: Observable<SensorReading[]>;

  constructor(private http: HttpClient) {
    // TODO: Handle this error
    this.readings$ = this.http.get<SensorReading[]>('/api/readings');
  }


  public getReadings(): Observable<SensorReading[]> {
    return this.readings$;
  }

}
