// TODO: Change these to enums if there are set values for them
export type SensorName = string;
export type SensorType = string;
export type SensorUnit = string;

export interface SensorReading {
  id: string;
  box_id: string;
  sensor_type: SensorType;
  unit: SensorUnit;
  name: SensorName;
  range_l: number;
  range_u: number;
  longitude: number;
  latitude: number;
  reading: number;
  reading_ts: string;
}
