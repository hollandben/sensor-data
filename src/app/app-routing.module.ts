import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ReadingsComponent} from './modules/readings/pages/readings.component';
import {PageNotFoundComponent} from './modules/page-not-found/page-not-found.component';


const routes: Routes = [
  { path: 'readings', component: ReadingsComponent },
  { path: '', redirectTo: '/readings', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }, ];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
