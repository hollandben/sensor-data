# Sensat

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.1.

#### Running the app

```bash
npm install                                 // install packages
npm run dev:ssr                             // run the app in dev mode
npm run build:ssr && npm run serve:ssr      // build and run app

ng test                                     // to run unit tests
ng e2e                                      // to run e2e tests
```

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
