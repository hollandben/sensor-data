#### Summary
I was given the choice of using Angular or React, so figured I'd use it as an opportunity to see what's changed in the Angular world... and it's a fair bit. I spent bits of time before the test trying to ingest information about what had changed so felt I had a reasonably good understanding to do the test.

I don't feel I made it very far with this test and I don't think it does a great job of showing my frontend skills, so figured I'd give a detailed explanation in here.

#### Architecture
I aimed to make the readings table be driven solely by configuration, leaving a generic template and making it easy to change in the future (add columns, make more sortable etc).

To mimic a real application, I switched on the server side rendering of the application so that I could provide an API endpoint for the sensor readings. I didn't do anything extra here than return the JSON file. The filtering/sorting is all done on the client at the moment, but I would definitely move it all to this server endpoint. It would make it a lot easier and faster to test the logic. It also reduces the  size of the network request (as it's sending a smaller payload) and requires less resources from the browser to keep all 8000+ records in memory let alone render them. Works fine on my Macbook but might not for a lower powered machine or phone/tablet.

#### Sorting
Used the built-in Material Table functionality for this.

#### Filtering
I spent a fair amount of time on this, probably too much.

My original idea was to provide dropdowns for Sensor Name and Sensor Type with populated options based on the data in the table. This wasn't a problem, but I got a little lost trying to combine the stream of events with the stream from the ReadingsService to do the filtering. In the end I used the built-in filtering from the Material Table as it got the job done, but a little sad I didn't get the Observable streams merged and working.

#### Testing
I didn't get round to writing any tests for the app, my main reason being I've limited time at the moment to learn the testing methodology in Angular.

My testing strategy would have been based on my experience with React:

- Unit test any logic
- Write component tests for smart components
- Write component tests for components that need to work together (component integration tests)
- Write some end-to-end tests for the main user flow, in this case being filtering or sorting
